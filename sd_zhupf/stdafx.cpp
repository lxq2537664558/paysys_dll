// stdafx.cpp : 只包括标准包含文件的源文件
// sd_zhupf.pch 将作为预编译头
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"

// TODO: 在 STDAFX.H 中引用任何所需的附加头文件，
//而不是在此文件中引用

string GetExtFullPath()
{
	string exe_path(256, NULL);
	GetModuleFileName(NULL, (PSTR)exe_path.c_str(), exe_path.length());
	exe_path = exe_path.c_str();
	return exe_path;
}

string GetExeFullDir()
{
	string exe_path = GetExtFullPath();
	string exe_dir = exe_path.substr(0, exe_path.find_last_of('\\') + 1);
	return exe_dir;
}

void LOGFILE(LPCTSTR lpszFormat, ...)
{
	va_list args;
	va_start(args, lpszFormat);
	int nBuf;
	static TCHAR szBuffer[800];
	SYSTEMTIME st;
	GetLocalTime(&st);

	DWORD dwTick = 0;
	wsprintf(szBuffer, "[%02d-%02d,%02d:%02d,%d.%d]: ", st.wMonth, st.wDay, st.wHour, st.wMinute, (int)(dwTick / 1000), (int)(dwTick % 1000));
	nBuf = vsprintf(szBuffer + lstrlen(szBuffer), lpszFormat, args);

	va_end(args);
	// was there an error? was the expanded string too long?
	lstrcat(szBuffer, "\n");

	string logfile = GetExeFullDir() + "paysys_zpf.log";
	FILE * pFile = fopen(logfile.c_str(), "a+t");
	if (pFile == NULL)	return;
	fwrite(szBuffer, sizeof(TCHAR), lstrlen(szBuffer), pFile);
	fclose(pFile);

	::OutputDebugString(szBuffer);
}