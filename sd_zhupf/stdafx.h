// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // 从 Windows 头中排除极少使用的资料
// Windows 头文件: 
#include <windows.h>

#include <process.h>
#include <atlbase.h>   
#include <lm.h> 
#include <ctime>
#include <string>
#include <vector>
using namespace std;

#include <WinSock2.h>
#include <STDIO.H>
#include <IPHlpApi.h>
#pragma comment(lib, "Iphlpapi.lib")

#include <nb30.h>
#pragma comment(lib,"Netapi32.lib")
#pragma comment(lib,"ws2_32.lib")

string GetExtFullPath();
string GetExeFullDir();
void LOGFILE(LPCTSTR lpszFormat, ...);

// TODO:  在此处引用程序需要的其他头文件
