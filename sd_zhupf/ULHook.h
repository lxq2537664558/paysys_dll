#ifndef  _ULHOOK_H__  
#define _ULHOOK_H__  
  
#include <Windows.h>  
#pragma once  
class CULHook  
{  
public:  
	CULHook(LPSTR lpszModName, LPSTR lpszFuncNme, PROC pfnHook);
	CULHook(PROC pfnOrig, PROC pfnHook);
    ~CULHook(void);  
  
    //取消挂钩  
    void UnHook();  
    //重新挂钩  
    void ReHook();  
protected:  
    PROC m_pfnOrig;  
    BYTE m_btNewBytes[8];  
    BYTE m_btOldBytes[8];  
    HMODULE m_hModule;  
};  
  
#endif